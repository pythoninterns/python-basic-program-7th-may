#Convert nested list into single list

list1 = [1,2,3,4,[2,34,6],[1,2,3]]
list2 = []

for i in list1:
    if type(i) == type(list1):      
        for j in i:
            list2.append(j)
    else:
        list2.append(i)

print(list2)        



#Fibonacci series pattern
size = int(input("How many rows you want to print:: "))

a,b = 0,1
for row in range(size):
    for col in range(row+1):
        print(a,end=" ")
        c=a+b
        a=b
        b=c
    print()
    
    
#Arithmatic Exception Handling (Divide by zero)
num1,num2=input("Enter two numbers(Divisor and Divident)::")
try:   
    a = num1/num2  
    print(a)   
except ArithmeticError:   
        print("Arithmetic Exception Occur.")  
else:   
    print("Success.")


#Ternary operation in python
num1, num2 = 112, 234
small = num1 if num1 < num2 else num2 
  
print(small) 


#Python program to implement *args and **kwargs  
# *args
def myfun(arg1, *argss): 
    print("First value :",arg1) 
    for arg in argss: 
        print("Next value through *args:", arg) 
  
myfun(1,2,3,'Hemraj','Hemu')

# **kwargs
def myfun(**kwargs):  
    for key, value in kwargs.items(): 
        print ("{} == {}".format(key, value)) 
  
myfun(a ='Hemu', b ='Hemraj', c ='Devil')  
